import {agent} from 'superagent'

export type TokenOption = {bearer: string}
export type BaseUrlOption = {baseUrl?: string}

export default function ({bearer, baseUrl= 'https://api.any4.io'}: TokenOption & BaseUrlOption) {
  return agent().use(request => {
    if (request.url.startsWith('/')) request.url = `${baseUrl}${request.url}`
    if (bearer) request.auth(bearer, {type: 'bearer'})
  })
}
