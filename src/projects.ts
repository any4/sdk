import request, {TokenOption, BaseUrlOption} from './request'
import {Any4Error, ServerError, NotAuthenticated} from './error'

export type CreateOptions = {name: string} & TokenOption & BaseUrlOption

export class ProjectExistsError extends Any4Error {
  constructor(public name: string, instance: string) {
    super('Project already exists', instance)
  }
}

export class InvalidProject extends Any4Error {
  constructor(public suggestions, instance) {
    super('Invalid project', instance)
  }
}

export async function create(options: CreateOptions): Promise<void> {
  await request(options).post('/v2/projects').send({name: options.name}).catch(error => {
    const {status, response: {body, header: {'x-any4-leaf-request-id': instance}}} = error
    if (status === 409) throw new ProjectExistsError(options.name, instance)
    if (status === 400) throw new InvalidProject(body.suggestions, instance)
    if (status === 401) throw new NotAuthenticated(instance)
    throw new ServerError(status, 'Server error', instance, ...body)
  })
}
