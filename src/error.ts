export abstract class Any4Error extends Error {
  protected constructor(message: string, public readonly instance: string) {
    super(message)
  }
}

export class ServerError extends Any4Error {
  public readonly properties

  constructor(public status: number, message: string, instance: string, ...properties) {
    super(message, instance)
    this.properties = properties
  }
}

export class NotAuthenticated extends Any4Error {
  constructor(instance: string) {
    super('Operation requires a valid token', instance)
  }
}
