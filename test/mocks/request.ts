import * as superagent from 'superagent'
import * as supermock from 'superagent-mock'
import {sandbox} from './sandbox'

export const fixtures = sandbox.stub()
export const post = sandbox.stub()

const mock = supermock(superagent, [{pattern: 'https://api.any4.io/(.*)', fixtures, post}])

after(() => {
  mock.unset()
})
