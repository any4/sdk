import {post} from './mocks'
import {projects} from '../src'
import 'should'

describe('Projects', () => {
  describe('create', () => {
    const name = 'foo', bearer = 'token', instance = '1'

    describe('when API returns created', () => {
      beforeEach(() => post.returns({status: 201, body: {name}}))

      it('should resolve', () => projects.create({name, bearer}))
    })

    describe('when API throws conflict', () => {
      beforeEach(() => post.throws({
        status: 409, response: {body: {reason: 'ProjectExists'}, header: {'x-any4-leaf-request-id': instance}}
      }))

      it('should throw project exists', () => projects.create({name, bearer})
        .should.be.rejectedWith({message: 'Project already exists', name, instance}))
    })

    describe('when API throws not authorized', () => {
      beforeEach(() => post.throws({
        status: 401, response: {body: {reason: 'NotAuthenticated'}, header: {'x-any4-leaf-request-id': instance}}
      }))

      it('should throw invalid token', () => projects.create({name, bearer})
        .should.be.rejectedWith({message: 'Operation requires a valid token', instance}))
    })
  })
})
